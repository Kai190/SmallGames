import datetime
from tkinter import *
from PIL import Image, ImageTk
from random import randint
from tkinter import messagebox


class Point:
    __slots__ = ['x', 'y']

    def __init__(self, x, y):
        self.x = x
        self.y = y

    def __getitem__(self, key):
        if key == 0:
            return self.x
        if key == 1:
            return self.y
        raise StopIteration

    def __str__(self):
        return "[x=" + str(self.x) + ",y=" + str(self.y) + "]"


DEFAULT_GAME_SIZE = Point(800, 600)
DEFAULT_PLAYER_SIZE = Point(100, 100)
current_enemy_images = []


class PauseMenu:
    def __init__(self, parent):
        self.top = Toplevel(parent)
        self.top.transient(parent)
        self.top.wm_attributes("-type", "splash")
        self.top.bind('<KeyPress>', up)
        self.top.geometry("+%d+%d" % (self.top.winfo_x()+50, self.top.winfo_y()+50) )

class Enemy:
    def __init__(self):
        self.v = None
        self.size = None
        self.canvas_id = None
        self.image_index = None
        self.spawn()

    def spawn(self, canvas_id=None):
        if self.canvas_id is not None:
            canvas.delete(canvas_id)
        if self.image_index is None:
            current_enemy_images.append(None)
            self.image_index = len(current_enemy_images) - 1

        self.size = randint(5, 100)
        img = ImageTk.PhotoImage(enemy_image.resize((self.size, self.size), Image.ANTIALIAS))
        current_enemy_images[self.image_index] = img

        x = 0
        y = 0
        border = randint(0, 3)  # 0 = top, 1 = right, 2 = bottom, 3 = left
        if border == 0:
            y -= self.size / 2
            x = randint(0, canvas.winfo_width())
            self.v = (randint(-4, 4), randint(1, 4))
        elif border == 1:
            x += canvas.winfo_width() + self.size / 2
            y = randint(0, canvas.winfo_height())
            self.v = (randint(-4, -1), randint(-4, 4))
        elif border == 2:
            y += canvas.winfo_height() + self.size / 2
            x = randint(0, canvas.winfo_width())
            self.v = (randint(-4, 4), randint(-4, -1))
        else:  # 3
            x -= self.size / 2
            y = randint(0, canvas.winfo_height())
            self.v = (randint(1, 4), randint(-4, 4))
        self.canvas_id = canvas.create_image(x, y, image=img)


def update():
    global frame_count
    global time_gifs
    global time_movement
    global count_spaceship
    global count_space
    global output_lbl
    if killed:
        messagebox.showinfo("Information", "You lost!")
        sys.exit(0)
    if datetime.datetime.now() - time_gifs > datetime.timedelta(microseconds=125000):
        time_gifs = datetime.datetime.now()
        count_spaceship = 0 if count_spaceship == 1 else count_spaceship + 1
        count_space = 0 if count_space == 10 else count_space + 1
        canvas.itemconfig(spaceship, image=spaceship_images[count_spaceship])
        canvas.itemconfig(background, image=space_images_resized[count_space])
    if not paused and datetime.datetime.now() - time_movement > datetime.timedelta(microseconds=16666):
        time_movement = datetime.datetime.now()
        canvas.move(spaceship, 5 * horizontal, 5 * vertical)
        move_enemies()
        output_lbl.config(text="Score: "+str(score))
    frame_count = 0 if frame_count == 60 else frame_count + 1
    main_window.after(5, lambda: update())


def move_enemies():
    global killed
    global score
    for index in range(0, len(enemies)):
        enemy = enemies[index]
        canvas.move(enemy.canvas_id, enemy.v[0], enemy.v[1])
        x = canvas.coords(enemy.canvas_id)[0]
        y = canvas.coords(enemy.canvas_id)[1]
        if (x <= 0 - enemy.size
                or x >= canvas.winfo_width() + enemy.size
                or y <= 0 - enemy.size
                or y >= canvas.winfo_height() + enemy.size):
            enemy.spawn(enemy.canvas_id)
            score += 1
        if not killed:
            killed = check_collison(enemy)


def create_enemies():
    for index in range(0, num_enemies):
        enemies.append(Enemy())


def up(event):
    global horizontal
    global vertical
    global paused
    ek = event.keysym
    if not paused:
        if ek == "Left":
            horizontal -= 0 if horizontal == -1 else 1
        if ek == "Right":
            horizontal += 0 if horizontal == 1 else 1
        if ek == "Up":
            vertical -= 0 if vertical == -1 else 1
        if ek == "Down":
            vertical += 0 if vertical == 1 else 1
        if ek == "Escape":
            paused = True
    else:
        if ek == "Escape":
            paused = False


def check_collison(enemy):
    # coords start at anchor, which is center, so we subtract half the size to get an edge point
    enemyl = Point(*canvas.coords(enemy.canvas_id))
    enemyl.x -= enemy.size / 2
    enemyl.y -= enemy.size / 2
    playerl = Point(*canvas.coords(spaceship))
    playerl.x -= DEFAULT_PLAYER_SIZE.x / 2
    playerl.y -= DEFAULT_PLAYER_SIZE.y / 2

    enemyr = Point(enemyl.x + enemy.size, enemyl.y + enemy.size)
    playerr = Point(playerl.x + DEFAULT_PLAYER_SIZE.x, playerl.y + DEFAULT_PLAYER_SIZE.y)

    if enemyl.x > playerr.x or playerl.x > enemyr.x:
        return False

    if enemyl.y > playerr.y or playerl.y > enemyr.y:
        return False

    return True


horizontal = 0
vertical = 0
frame_count = 0
num_enemies = 5
enemies = []
paused = True
killed = False
pause_menu = None
score = 0

main_window = Tk()
main_window.title("Dodger")
main_window.geometry(str(DEFAULT_GAME_SIZE.x) + "x" + str(DEFAULT_GAME_SIZE.y))
main_window.bind('<KeyPress>', up)
main_window.config(cursor="none")
main_window.resizable(False, False)

canvas = Canvas(main_window)
canvas.pack(expand=YES, fill=BOTH)

space_images = []
space_images_resized = []
for i in range(0, 12):
    space_images.append(Image.open(f'assets/expanded/Space{i:02d}.png'))
    space_images_resized.append(ImageTk.PhotoImage(space_images[i].resize(DEFAULT_GAME_SIZE, Image.ANTIALIAS)))

spaceship_images = []
for i in range(0, 2):
    spaceship_images.append(
        ImageTk.PhotoImage(
            Image.open(f'assets/expanded/spaceship{i:1d}.png').resize(DEFAULT_PLAYER_SIZE, Image.ANTIALIAS)))

enemy_image = Image.open('assets/monster.jpg')

background = canvas.create_image(0, 0, image=space_images_resized[0], anchor=NW)
spaceship = canvas.create_image(DEFAULT_GAME_SIZE.x / 2, DEFAULT_GAME_SIZE.y / 2, image=spaceship_images[0])
create_enemies()

output_lbl = Label(main_window, text="Press Escape Key to start")
output_lbl.pack()

time_gifs = datetime.datetime.now()
time_movement = datetime.datetime.now()
count_spaceship = 0
count_space = 0

update()
mainloop()
